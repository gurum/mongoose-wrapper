# - Try to find Jsoncpp
#
# The following variables are optionally searched for defaults
# JSONCPP_ROOT_DIR:            Base directory where all JSONCPP components are found
#
# The following are set after configuration is done:
#  JSONCPP_FOUND
#  JSONCPP_INCLUDE_DIRS
#  JSONCPP_LIBRARIES
#  JSONCPP_LIBRARYRARY_DIRS

include(FindPackageHandleStandardArgs)

set(URIPARSER_ROOT_DIR "" CACHE PATH "Folder contains Uriparser")

find_path(URIPARSER_INCLUDE_DIR uriparser/Uri.h
  PATHS ${URIPARSER_ROOT_DIR})

find_library(URIPARSER_LIBRARY uriparser
    PATHS ${URIPARSER_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_package_handle_standard_args(uriparser DEFAULT_MSG URIPARSER_INCLUDE_DIR URIPARSER_LIBRARY)

if(URIPARSER_FOUND)
  set(URIPARSER_INCLUDE_DIRS ${URIPARSER_INCLUDE_DIR})
  set(URIPARSER_LIBRARIES ${URIPARSER_LIBRARY})
  message(STATUS "Found uriparser    (include: ${URIPARSER_INCLUDE_DIRS}, library: ${URIPARSER_LIBRARIES})")
  mark_as_advanced(URIPARSER_ROOT_DIR URIPARSER_LIBRARY_RELEASE URIPARSER_LIBRARY_DEBUG
                                 URIPARSER_LIBRARY URIPARSER_INCLUDE_DIR)
endif()
