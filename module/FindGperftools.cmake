# - Try to find Gperftools
#
# The following variables are optionally searched for defaults
#  GPERFTOOLS_ROOT_DIR:            Base directory where all SimpleIO components are found
#
# The following are set after configuration is done:
#  GPERFTOOLS_FOUND
#  GPERFTOOLS_INCLUDE_DIRS
#  GPERFTOOLS_LIBRARIES
#  GPERFTOOLS_LIBRARYRARY_DIRS
include(FindPackageHandleStandardArgs)
set(GPERFTOOLS_ROOT_DIR "$ENV{GPERFTOOLS_ROOT_DIR}" CACHE PATH "Folder contains GPERFTOOLS gperftools")

find_path(GPERFTOOLS_LIBRARYRARY_DIRS libprofiler.so
    PATHS ${GPERFTOOLS_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_library(GPERFTOOLS_PROFILER_LIBRARY profiler
    PATHS ${GPERFTOOLS_ROOT_DIR}
    PATH_SUFFIXES lib lib64)

find_library(GPERFTOOLS_TCMALLOC_LIBRARY tcmalloc
    PATHS ${GPERFTOOLS_ROOT_DIR}
    PATH_SUFFIXES lib lib64)


find_package_handle_standard_args(GPERFTOOLS DEFAULT_MSG GPERFTOOLS_PROFILER_LIBRARY GPERFTOOLS_TCMALLOC_LIBRARY GPERFTOOLS_LIBRARYRARY_DIRS)
if(GPERFTOOLS_FOUND)
  set(GPERFTOOLS_INCLUDE_DIRS ${GPERFTOOLS_INCLUDE_DIR})
  set(GPERFTOOLS_LIBRARIES ${GPERFTOOLS_PROFILER_LIBRARY} ${GPERFTOOLS_TCMALLOC_LIBRARY} "-Wl,--no-as-needed -lprofiler -Wl,--as-needed")
  message(STATUS "Found gperftools (include: ${GPERFTOOLS_INCLUDE_DIRS}, library: ${GPERFTOOLS_LIBRARIES})")
  mark_as_advanced(GPERFTOOLS_ROOT_DIR GPERFTOOLS_LIBRARY_RELEASE GPERFTOOLS_LIBRARY_DEBUG
                                 GPERFTOOLS_LIBRARY GPERFTOOLS_INCLUDE_DIR)
endif()
