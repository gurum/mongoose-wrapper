# - Try to find Jsoncpp
#
# The following variables are optionally searched for defaults
# JSONCPP_ROOT_DIR:            Base directory where all JSONCPP components are found
#
# The following are set after configuration is done:
#  JSONCPP_FOUND
#  JSONCPP_INCLUDE_DIRS
#  JSONCPP_LIBRARIES
#  JSONCPP_LIBRARYRARY_DIRS

#include(FindPackageHandleStandardArgs)

set(OPENSSL_ROOT_DIR "" CACHE PATH "Folder contains OpenSSL")

find_path(OPENSSL_INCLUDE_DIR openssl/ssl.h
  PATHS ${OPENSSL_ROOT_DIR})


find_library(OPENSSL_SSL_LIBRARY ssl
    PATHS ${OPENSSL_ROOT_DIR}
    PATH_SUFFIXES ssl/lib lib64)

find_library(OPENSSL_CRYPTO_LIBRARY crypto
    PATHS ${OPENSSL_ROOT_DIR}
    PATH_SUFFIXES ssl/lib lib64)

find_package_handle_standard_args(openssl DEFAULT_MSG OPENSSL_INCLUDE_DIR OPENSSL_SSL_LIBRARY OPENSSL_CRYPTO_LIBRARY)

if(OPENSSL_FOUND)
  set(OPENSSL_INCLUDE_DIRS ${OPENSSL_INCLUDE_DIR})
  set(OPENSSL_LIBRARIES ${OPENSSL_SSL_LIBRARY} ${OPENSSL_CRYPTO_LIBRARY})
  message(STATUS "Found openssl    (include: ${OPENSSL_INCLUDE_DIRS}, library: ${OPENSSL_LIBRARIES})")
  mark_as_advanced(OPENSSL_ROOT_DIR OPENSSL_LIBRARY_RELEASE OPENSSL_LIBRARY_DEBUG
                                 OPENSSL_CRYPTO_LIBRARY OPENSSL_SSL_LIBRARY OPENSSL_INCLUDE_DIR)
endif()
