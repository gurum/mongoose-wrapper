SHELL := /bin/bash

BUILD_ROOT ?= ${shell pwd}

VENDOR_DIR=$(BUILD_ROOT)

ifeq ($(ARCH),arm64)
TARGET_DIR?=$(BUILD_ROOT)/target.$(ARCH)
else
TARGET_DIR?=$(BUILD_ROOT)/target
endif

TARGET_INC_DIR = $(TARGET_DIR)/usr/local/include
TARGET_LIB_DIR = $(TARGET_DIR)/usr/local/lib

export PLATFORM?=$(shell echo `uname -s`)
export PLATFORM_LOWER?=$(shell echo ${PLATFORM} | tr '[:upper:]' '[:lower:]')

BUILD_DEPS=jsoncpp uriparser openssl wolfssl

ifeq ($(ARCH),arm64)
#CONFIG_ARGS=-DCMAKE_PREFIX_PATH=$(TARGET_DIR)/usr/local -DCMAKE_SYSROOT=$(SDKTARGETSYSROOT) -DCMAKE_SYSTEM_LIBRARY_PATH=$(SDKTARGETSYSROOT)/usr/lib64
CONFIG_ARGS=-DCMAKE_PREFIX_PATH=$(TARGET_DIR)/usr/local
CONFIG_ARGS+=-DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_LIBS=OFF
OPENSSL_HOST_ARCH=linux-aarch64
HOST_ARCH:=aarch64-agl-linux
else ifeq ($(ARCH),mips)
HOST_ARCH:=mipsel-linux

# TODO: CONFIG_ARGS:=
OPENSSL_HOST_ARCH=linux-mips32
else
CONFIG_ARGS:=-DCMAKE_PREFIX_PATH=$(TARGET_DIR)/usr/local
	ifeq ($(PLATFORM_LOWER),darwin)
		OPENSSL_HOST_ARCH=darwin64-x86_64-cc
		BUILD_DEPS=
		CONFIG_ARGS+=-DBUILD_SHARED_LIBS=OFF -DBUILD_STATIC_LIBS=ON
	else
		OPENSSL_HOST_ARCH=linux-ia64
		CONFIG_ARGS+=-DBUILD_SHARED_LIBS=OFF -DBUILD_STATIC_LIBS=ON
	endif
endif

CONFIG_ARGS+=-DUSE_SYSLOG=ON
CONFIG_ARGS+=-DUSE_OPENSSL=ON


openssl_version:=1.0.2o
#openssl_version:=1.0.2t

ifeq ($(ARCH),arm64)
BUILD_DIR=build.$(ARCH)
else
BUILD_DIR=build
endif

.PHONY : build install deps

all: configure build restclient

build:
	@echo "[$@]"
	cd $(VENDOR_DIR)/$(BUILD_DIR) ;	\
	$(MAKE) VERBOSE=1 ;	\
	$(MAKE) install DESTDIR=$(TARGET_DIR)

clean:
	@echo "[$@]"
	@ $(RM) -rf $(VENDOR_DIR)/$(BUILD_DIR)
	@echo "[$@] completed"

distclean: clean
	@echo "[$@]"
	@ $(RM) -rf $(TARGET_DIR)
	@echo "[$@] completed"

$(TARGET_DIR):
	@echo "[$@]"
	@mkdir -p $@
	@echo "[$@] completed"

restclient:
	@echo "[$@]"
	@if ! [ -f $(TARGET_DIR)/usr/local/lib/librestclient.so ]; then \
		if ! [ -d .tmp/$@ ]; then \
		  git clone https://gitlab.com/gurum/restclient.git .tmp/$@ ;	\
		fi;	\
		mkdir -p .tmp/$@/build; \
		cd .tmp/$@/build; cmake $(CONFIG_ARGS) ..; \
		make; \
		make install DESTDIR=$(TARGET_DIR);	\
		cd -;	\
	fi
	@echo "[$@] completed"

wolfssl:
	@echo "[$@]"
	@if ! [ -f $(TARGET_DIR)/usr/local/lib/libwolfssl.so ]; then \
		if ! [ -d .tmp/$@ ]; then \
		  git clone --branch v3.13.0-stable https://github.com/wolfSSL/wolfssl.git .tmp/$@ ;	\
		fi;	\
		cd .tmp/$@;	\
		./autogen.sh; \
		./configure --host=$(HOST_ARCH);	\
		make && make install DESTDIR=$(TARGET_DIR);	\
	fi
	@echo "[$@] completed"

# 		#./configure --host=$(HOST_ARCH) --enable-opensslcoexist --enable-nginx \
# 			CFLAGS="-I$(TARGET_DIR)/usr/local/ssl/include/openssl -DTEST_OPENSSL_COEXIST" \
#         	LDFLAGS="-L$(TARGET_DIR)/usr/local/ssl/lib -lcrypto


openssl:
	@echo "[$@]"
	@if ! [ -f $(TARGET_DIR)/usr/local/ssl/lib/libssl.a ]; then \
		wget https://www.openssl.org/source/openssl-$(openssl_version).tar.gz -P .tmp/ ;	\
		tar xf .tmp/openssl-$(openssl_version).tar.gz -C .tmp/;	\
		cd .tmp/openssl-$(openssl_version);	\
		./Configure \
			$(OPENSSL_HOST_ARCH)	\
			enable-tls \
			no-zlib \
			no-asm \
			threads \
			no-idea \
			no-mdc2 \
			no-rc5 \
			no-ripemd && \
		make depend && make CC="$(CC)" && make install_sw INSTALL_PREFIX=$(TARGET_DIR);	\
	fi
	@echo "[$@] completed"

gperftools:
	@echo "[$@]"
	@if ! [ -f $(TARGET_LIB_DIR)/libtcmalloc.a ]; then \
		if ! [ -d .tmp/$@ ]; then \
			wget https://github.com/gperftools/gperftools/releases/download/gperftools-2.7/gperftools-2.7.tar.gz -P .tmp/ ;	\
			tar xf .tmp/gperftools-2.7.tar.gz -C .tmp/;	\
		fi;	\
		cd .tmp/gperftools-2.7;	\
		./autogen.sh ;./configure --host=$(HOST_ARCH) ; make; make install DESTDIR=$(TARGET_DIR);	\
	fi
	@echo "[$@] completed"


jsoncpp:
	@echo "[$@]"
	@if  [ ! -f $(TARGET_LIB_DIR)/libjsoncpp.so ] &&  [ ! -f $(TARGET_LIB_DIR)/libjsoncpp.a ]; then \
		if ! [ -d ./.tmp ]; then \
		  mkdir .tmp;	\
		fi;	\
		if ! [ -d .tmp/$@ ]; then \
			git clone --branch 0.10.7 https://github.com/open-source-parsers/jsoncpp.git .tmp/$@;	\
		fi;	\
		mkdir -p .tmp/$@/build; \
		cd .tmp/$@/build; cmake $(CONFIG_ARGS) -DJSONCPP_WITH_TESTS=OFF ..; \
		make; \
		make install DESTDIR=$(TARGET_DIR);	\
		cd -;	\
	fi
	@echo "[$@] completed"

uriparser:
	@echo "[$@]"
	@if  [ ! -f $(TARGET_LIB_DIR)/liburiparser.a ] && [ ! -f $(TARGET_LIB_DIR)/liburiparser.so ]; then \
		if ! [ -d .tmp/$@ ]; then \
			git clone https://github.com/uriparser/uriparser.git .tmp/$@;	\
		fi	;	\
		mkdir -p .tmp/$@/build; \
		cd .tmp/$@/build; cmake $(CONFIG_ARGS) -DURIPARSER_BUILD_TESTS=OFF -DURIPARSER_BUILD_DOCS=OFF ..; \
		make; \
		make install DESTDIR=$(TARGET_DIR);	\
	fi
	@echo "[$@] completed"


deps: $(BUILD_DEPS)
	@echo "[$@]"
	@echo "[$@] completed"

configure: deps
	@echo "[$@]"
	@if ! [ -d $(VENDOR_DIR)/$(BUILD_DIR) ]; then \
		mkdir $(VENDOR_DIR)/$(BUILD_DIR);	\
		echo ${CONFIG_ARGS} ;	\
		cd $(VENDOR_DIR)/$(BUILD_DIR) && cmake ${CONFIG_ARGS} .. ; \
	fi
	@echo "[$@] completed"

install: build
	@echo "[$@]"
	@if [[ ! -z "${TARGET_BOARD_IP}" ]]; then       \
		rsync -avzh $(TARGET_DIR)/usr/local/bin/* root@${TARGET_BOARD_IP}:/usr/local/bin/; \
		rsync -avzh $(TARGET_DIR)/usr/local/lib/*.so* root@${TARGET_BOARD_IP}:/usr/local/lib/; \
	else    \
		echo "You need set TARGET_BOARD_IP.";   \
	fi
	@echo "[$@] completed"
