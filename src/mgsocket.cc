/*
 * mgsocket.cc
 *
 *  Created on: Jun 10, 2013
 *      Author: buttonfly
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <string>
#include <iostream>
#include <stdint.h>
#include <arpa/inet.h>

#include "webservice/mgsocket.h"
#include "mongoose.h"

namespace gurum {

MGSocket::MGSocket(struct mg_connection* conn)
: _conn(conn)
{
}

MGSocket::~MGSocket()
{
	// TODO Auto-generated destructor stub
}

std::string MGSocket::Read()
{
	struct mg_connection * conn = (struct mg_connection *)_conn;
	return std::string(conn->content, conn->content_len);
}

/**
 * @return 0: is 0, -1: please disconnect
 */

int MGSocket::Write(const std::string& msg)
{
//	return mg_websocket_write((struct mg_connection *)m_conn, 1, msg.c_str(), msg.length());
	return Write(msg.c_str(), msg.length());
}

int MGSocket::Write(const char* buf, size_t len)
{
//	return mg_websocket_write((struct mg_connection *)m_conn, 1, buf, len);
	mg_send_data((struct mg_connection *)_conn,  (const void*) buf, (int) len);
	return len;
}

std::string MGSocket::RemoteIp(void* c)
{
	struct mg_connection * conn = (struct mg_connection *) c;
	return std::string(conn->remote_ip);
}

void MGSocket::SetHeader(const char* name, const char* v)
{
	mg_send_header((struct mg_connection *) _conn, name, v);
}

std::string MGSocket::Get(std::string name)
{
	char tmp[128] = {0,};
	mg_get_var((struct mg_connection *) _conn, name.c_str(), tmp, sizeof(tmp));
	return std::string(tmp);
}

} /* namespace k */
