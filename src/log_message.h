
#ifndef GURUM_BASE_LOGGING_LOG_MESSAGE_H_
#define GURUM_BASE_LOGGING_LOG_MESSAGE_H_

#if defined(USE_SYSLOG)
#undef USE_VW_VLOG
#endif

#if defined(USE_GLOG)
#include <glog/logging.h>
#elif defined(USE_SYSLOG)

#if defined(LOG)
#undef LOG
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sstream>
#include <ostream>
#include <syslog.h>
#include <string.h>
#include <pthread.h>
#include <chrono>

typedef int LogSeverity;
const LogSeverity VERBOSE = -1;
const LogSeverity INFO = LOG_INFO;
const LogSeverity WARNING = LOG_WARNING;
const LogSeverity ERROR = LOG_ERR;
const LogSeverity FATAL = LOG_ERR;

namespace base {

namespace logging {

class LogMessage {
public:
  LogMessage(LogSeverity severity, const std::string &file, const std::string &func, int line)
  : severity_(severity), file_(file), func_(func), line_(line) {
    static auto start = std::chrono::high_resolution_clock::now();
    stream() << "[" << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count() << "]"
        << "[" << std::hex <<  pthread_self() << "]" << std::dec;
  }

  ~LogMessage() {
    syslog(severity_, "%s",stream_.str().c_str());
  }

  std::ostream &stream() {return stream_;}

private:
  std::ostringstream stream_;
  LogSeverity severity_;
  const std::string file_;
  const std::string func_;
  const unsigned int line_;
};

} // namespace logging

} // namespace base

#define LOG(verbose_level) ::base::logging::LogMessage(verbose_level, __FILE__, __func__, __LINE__).stream()


#else


#if defined(LOG)
#undef LOG
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sstream>
#include <ostream>
#include <syslog.h>
#include <string.h>
#include <pthread.h>
#include <chrono>
#include <iomanip>
#include <assert.h>
#if defined(__APPLE__)
#include <libgen.h>
#endif

typedef int LogSeverity;
const LogSeverity VERBOSE = -1;
const LogSeverity INFO = LOG_INFO;
const LogSeverity WARNING = LOG_WARNING;
const LogSeverity ERROR = LOG_ERR;
const LogSeverity FATAL = LOG_ERR;

namespace base {

namespace logging {

class LogMessage {
public:
  LogMessage(LogSeverity severity, const std::string &file, const std::string &func, int line)
  : severity_(severity), file_(basename(file.c_str())), func_(func), line_(line) {
    static auto start = std::chrono::high_resolution_clock::now();
    stream()
        << "["
        <<  std::setfill ('0') << std::setw (10)
        << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count()
        << "]"
        << "["
        <<  std::setfill ('0') << std::setw (10)
        << std::hex <<  pthread_self()
        << "] " << std::dec
        << file_ << ":" <<line_ << "] ";
  }

  ~LogMessage() {
//    std::cerr << stream_.str() << std::endl;
    syslog(severity_, "%s", stream_.str().c_str());
 }

  std::ostream &stream() {return stream_;}

private:
  std::ostringstream stream_;
  LogSeverity severity_;
  const std::string file_;
  const std::string func_;
  const unsigned int line_;
};

} // namespace logging

} // namespace base

#define LOG(verbose_level) ::base::logging::LogMessage(verbose_level, __FILE__, __func__, __LINE__).stream()
#define CHECK_NOTNULL(x) assert(x)

#endif

#endif // GURUM_BASE_LOGGING_LOG_MESSAGE_H_
