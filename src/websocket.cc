/*
 * websocket.cpp
 *
 *  Created on: Jun 10, 2013
 *      Author: buttonfly
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <string>
#include <iostream>
#include <stdint.h>
#include <arpa/inet.h>

#include "webservice/websocket.h"
#include "mongoose.h"
#include "log_message.h"

namespace gurum {

WebSocket::WebSocket(struct mg_connection* conn)
: _conn(conn){
}

WebSocket::~WebSocket(){
}

std::string WebSocket::Read(){
	struct mg_connection * conn = (struct mg_connection *)_conn;
	return std::string(conn->content, conn->content_len);
}

/**
 * @return 0: is 0, -1: please disconnect
 */

int WebSocket::Write(const std::string& msg){
	return Write(msg.c_str(), msg.length());
}

int WebSocket::Write(const char* buf, size_t len){
	return mg_websocket_write((struct mg_connection *)_conn, 1, buf, len);
}

std::string WebSocket::RemoteIp(void* c){
	struct mg_connection * conn = (struct mg_connection *) c;
	return std::string(conn->remote_ip);
}

} /* namespace k */
