/*
 * HttpQuery.cc
 *
 *  Created on: Oct 16, 2015
 *      Author: buttonfly
 */

#include <uriparser/Uri.h>
#include <string>

#include "webservice/http_query.h"
#include "mongoose.h"
#include <memory>
#include <sstream>
#include <ostream>


namespace gurum {

HttpQuery::HttpQuery(const char* query)
: _query(query){}

HttpQuery::~HttpQuery() {}

auto HttpQuery::Parse() -> bool {

	std::string tmp("urn://tmp?");
	tmp.append(_query);

	UriParserStateA state;
  UriUriA uriA;

  auto uri = tmp.c_str();

  state.uri = &uriA;
  if (uriParseUriA(&state, uri) != URI_SUCCESS) {
          /* Failure */
    uriFreeUriMembersA(&uriA);
    fprintf(stderr, "failed to <uriParseUriA>: %s\n", uri);
    return NO;
  }

  UriQueryListA * lstQuery = NULL;
  int count = 0;
  uriDissectQueryMallocA(&lstQuery, &count, uriA.query.first, uriA.query.afterLast);
  for(UriQueryListA* query = lstQuery; query; query = query->next) {
   _kv[query->key] = query->value;
   fprintf(stderr, "[ %s : %s ]\n", query->key, query->value);
  }
  uriFreeQueryListA(lstQuery);
 	uriFreeUriMembersA(&uriA);
 	return YES;
}

auto HttpQuery::ToJson() -> std::string {
	std::string fmt("{");
	for(auto pos = _kv.begin(); pos != _kv.end(); ++pos) {
		if(pos != _kv.begin()) fmt.append(",");
		fmt.append("\"");
		fmt.append(pos->first);
		fmt.append("\"");

		fmt.append(":");

		fmt.append("\"");
		fmt.append(pos->second);
		fmt.append("\"");
	}
	fmt.append("}");
	return fmt;
}

auto HttpQuery::REST_Parse(Json::Value &root, struct mg_connection * conn) -> bool {
	std::string msg;

//	conn->request_method //GET, POST...
	std::string method(conn->request_method);
	if(method.compare("GET")==0) {
		if(conn->query_string) {
			char  buf[2048] = {0};
			mg_url_decode((const char*) conn->query_string, strlen(conn->query_string), buf, sizeof(buf), 0);
			msg = buf;
			HttpQuery query(buf);
			query.Parse();
			msg = query.ToJson();
		}
	}
	else {
		msg = conn->content;
	}

	auto at = msg.rfind("}");
	if(at == std::string::npos) {
		return "error";
	}

	msg = msg.substr(0, at+1);

#if deprecated
	Json::Reader reader;
	return reader.parse(msg, root);
#else
	Json::CharReaderBuilder builder;
	std::unique_ptr<Json::CharReader> reader {builder.newCharReader()};
	std::string errors;

	return reader->parse(msg.c_str(), msg.c_str() + msg.size(), &root, &errors);
#endif
}

auto HttpQuery::REST_Response(Json::Value &res, struct mg_connection * conn) -> int {
#if deprecated
	Json::FastWriter writer;
	auto body = writer.write(res);
#else
	Json::StreamWriterBuilder builder;
	builder["commentStyle"] = "None";
	builder["indentation"] = "   ";  // or whatever you like
	std::unique_ptr<Json::StreamWriter> writer{builder.newStreamWriter()};

  std::ostringstream stream;
	writer->write(res, &stream);
	auto body = stream.str();
#endif

	gurum::MGSocket sck(conn);
	auto v = sck.Get("id");

	sck.SetHeader("Access-Control-Allow-Origin", "*");
	return sck.Write(body);
}

} /* namespace gurum */
