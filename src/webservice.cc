/*
 * webservice.cc
 *
 *  Created on: Jun 8, 2013
 *      Author: buttonfly
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <string>
#include <iostream>
#include "mongoose.h"
#include <json/json.h>

#include "webservice/webservice.h"
#include "webservice/websocket.h"
#include <arpa/inet.h>

#include "log_message.h"

using namespace std;

namespace gurum {

WebService::WebService() : _context(NULL){}

WebService::~WebService(){
	_thread.join();
}

int WebService::init(void){
	_stopped = false;
	return 0;
}

int WebService::ReadAndDispatch() {
	time_t current_timer = 0, last_timer = time(NULL);
	mg_poll_server((struct mg_server *)_context, 1000);
	current_timer = time(NULL);
	if (current_timer - last_timer > 0) {
		last_timer = current_timer;
		mg_iterate_over_connections((struct mg_server *)_context, (int (*)(mg_connection *, enum mg_event))iterateCallback, &current_timer);
	}
	return 0;
}

int WebService::Start(void){
	_thread = std::thread([&](){
		for (;!_stopped;) {
			ReadAndDispatch();
		}
	});
	return 0;
}

int WebService::Init(const char *options[]){
	_context = mg_create_server(this, (int (*)(mg_connection *, enum mg_event))WebService::eventHandler);
	for(int i = 0; options[i] != NULL; i+=2) {
		//printf("{ %s : %s }\n", options[i], options[i+1]);
		const char* msg = mg_set_option((struct mg_server *)_context, options[i],  options[i+1]);
		if(msg) {
		  LOG(WARNING) << " " << msg;
		}
	}
	LOG(INFO) << "Started on port: " << mg_get_option((struct mg_server *)_context, "listening_port");
	return 0;
}

int WebService::Stop(void){
	_stopped = true;
	mg_destroy_server((struct mg_server **)&_context);
	return 0;
}

void WebService::RegisterService(const std::string &url, OnService callback) {
  std::lock_guard<std::recursive_mutex> lock(lck_);
  services_[url] = callback;
}

void WebService::RegisterService(const char *url, OnService callback) {
  RegisterService(std::string(url), callback);
}

void WebService::UnregisterService(const std::string &url) {
  std::lock_guard<std::recursive_mutex> lock(lck_);
  services_.erase(url);
}

int WebService::exec(const std::string &url, struct mg_connection *conn) {
  std::lock_guard<std::recursive_mutex> lock(lck_);
  auto callback = services_[url];
  if(! callback)
    return -1;

  return callback(conn, url);
}


//int WebService::Add(HttpService* s)
//{
//	s->bind(_httpserviceHandler);
//	return 0;
//}
//
//int WebService::Remove(HttpService* s)
//{
//	s->unbind(_httpserviceHandler);
//	return 0;
//}


int WebService::Response(void *conn, std::string& msg, uint8_t key[4]){
	// TODO
	int len = msg.length();
	if(len<=0)
		return -1;

	unsigned char* buf = new unsigned char[len+1];
	memset(buf, 0, len + 1);
	buf[0] = 0x81;  // text, FIN set
	buf[1] = len & 0x7F;

	for (int i = 0; i < len; i++) {
		buf[i + 2] = msg.c_str()[i];
	}

	mg_write((struct mg_connection*) conn, buf, 2 + len);

	delete[] buf;
	return 0;
}

int WebService::eventHandler(void *c, int e){
	struct mg_connection * conn = (struct mg_connection *)c;
	enum mg_event event = (enum mg_event) e;
	if(event == MG_CONNECT) {
		WebService* context = static_cast<WebService*>(conn->server_param);
		return MG_TRUE;
	}
	else if (event == MG_REQUEST) {
		if (conn->is_websocket) {
			WebSocket websocket(conn);
			std::string req = websocket.Read();

			WebService* context = static_cast<WebService*>(conn->server_param);
			if(req.length() == 0) {
				LOG(WARNING) << " " << conn->remote_ip << " invalid request (length is 0)";
				return MG_FALSE;
			}
			#if JSONRPC
			std::string res = context->process(req, (void*) conn);
			int n = websocket.write(res);
			return (n==4 && !res.compare("exit")) ? MG_FALSE : MG_TRUE;
			#endif // JSONRPC : TODO
		}
		else {
#if REST_SUPPORT
#if deprecated
      WebService* context = static_cast<WebService*>(conn->server_param);
      if(context->_httpserviceHandler.Exec(conn->uri, conn) == 0) {
				return MG_TRUE;
			}
			else {
				//TODO://
			}
#else
      WebService* context = static_cast<WebService*>(conn->server_param);
      if(! context->exec(conn->uri, conn)) return MG_TRUE;
#endif
#else
		    mg_send_header(conn, "Content-Type", "text/html");
			mg_send_data(conn, index_html, index_size);
			SYSLOG("This is not a websocket");
#endif

			// It must be false. if not, it doesn't work.
		    return MG_FALSE;
		}
	}
	else if (event == MG_AUTH) {
		return MG_TRUE;
	}
	else if (event == MG_CLOSE) {
    WebService* context = static_cast<WebService*>(conn->server_param);
    if (conn->is_websocket) {
			LOG(INFO) << " " << conn->remote_ip << " websocket closed";
			if(context->on_websocket_disconnected_) {
			  context->on_websocket_disconnected_(conn);
			}
			context->fireSocketClosedEvent(conn);
			return MG_TRUE; // It's gonna be ignore, anyway.
		}
	}
	else if(event == MG_WS_HANDSHAKE) {
    WebService* context = static_cast<WebService*>(conn->server_param);
    LOG(INFO) << " " << conn->remote_ip << " websocket connected";
    if(context->on_websocket_connected_) {
      context->on_websocket_connected_(conn);
    }
	}

	return MG_FALSE;
}

int WebService::iterateCallback(void *c, int e){
	struct mg_connection * conn = (struct mg_connection *)c;
	enum mg_event event = (enum mg_event) e;

	if (event == MG_POLL && conn->is_websocket) {
		char buf[20];
		int len = snprintf(buf, sizeof(buf), "%lu",
				(unsigned long) * (time_t *) conn->callback_param);
		mg_websocket_write(conn, 1, buf, len);
	}
	return MG_TRUE;
}

int WebService::fireSocketClosedEvent(void* conn){
#if JSONRPC
	for(std::list<IPlugin*>::iterator pos = _plugins.begin(); pos != _plugins.end(); ++pos) {
		IPlugin* p = (IPlugin*)*pos;
		if(p) {
			p->socketClosed(conn);
		}
	}
#endif
	return 0;
}

} /* namespace k */
