cmake_minimum_required(VERSION 3.0)


if (POLICY CMP0042)
  cmake_policy (SET CMP0042 NEW)
endif (POLICY CMP0042)

if (POLICY CMP0063)
  cmake_policy (SET CMP0063 NEW)
endif (POLICY CMP0063)

project (webservice)


# CMakeLists for libwebservice
# Run with -DTEST=ON to build unit tests
#option(TEST "Built unit tests" OFF)

option(USE_OPENSSL "Use OpenSSL" OFF)
option(USE_SYSLOG "Use Syslog" OFF)
option(BUILD_SHARED_LIBS "Build webservice_lib as a shared library." OFF)
option(BUILD_STATIC_LIBS "Build webservice_lib static library." ON)

FIND_PACKAGE (Threads)
set(LIBS ${LIBS} ${CMAKE_THREAD_LIBS_INIT})


set (CMAKE_CXX_STANDARD 17)


set(CMAKE_MODULE_PATH
    ${CMAKE_SOURCE_DIR}/module
    ${CMAKE_MODULE_PATH})

set(webservice_base_dir ${CMAKE_CURRENT_LIST_DIR})
set(webservice_src_dir ${webservice_base_dir}/src/)
set(webservice_inc_dir ${webservice_base_dir}/include)
set(webservice_script_dir ${webservice_base_dir}/script)
set(webservice_lib_name "webservice")

set(webservice_source_files
  ${webservice_src_dir}/webservice.cc
  ${webservice_src_dir}/websocket.cc
  ${webservice_src_dir}/http_query.cc
  ${webservice_src_dir}/mgsocket.cc
  ${webservice_src_dir}/mongoose.c
)

set(webservice_header_files
  ${webservice_inc_dir}/webservice/webservice.h
  ${webservice_inc_dir}/webservice/websocket.h
  ${webservice_inc_dir}/webservice/mgsocket.h
  ${webservice_inc_dir}/webservice/http_query.h
  ${webservice_inc_dir}/webservice/mongoose.h
  ${webservice_inc_dir}/webservice/config.h
)

set(webservice_script_files
  ${webservice_script_dir}/server-gen-keys.sh
  ${webservice_script_dir}/client-install-cert.sh
  ${webservice_script_dir}/client-update-nss.sh
  ${webservice_script_dir}/start
)

configure_file ("${webservice_inc_dir}/webservice/config.h.in"
                "${webservice_inc_dir}/webservice/config.h" )


find_package(Jsoncpp REQUIRED)
include_directories(${JSONCPP_INCLUDE_DIRS})
set(LIBS ${LIBS} ${JSONCPP_LIBRARY})

find_package(Uriparser REQUIRED)
include_directories(${URIPARSER_INCLUDE_DIRS})
set(LIBS ${LIBS} ${URIPARSER_LIBRARY})

if (USE_OPENSSL)
#set(OPENSSL_ROOT_DIR ${SYSROOT})
#if(OPENSSL_FOUND)
find_package(OpenSSL REQUIRED)
include_directories(${OPENSSL_INCLUDE_DIRS})
set(LIBS ${LIBS} ${OPENSSL_LIBRARIES} ${CMAKE_DL_LIBS})
add_definitions(-DNS_ENABLE_SSL)
#endif(OPENSSL_FOUND)
endif(USE_OPENSSL)

if (USE_SYSLOG)
add_definitions(-DUSE_SYSLOG)
endif(USE_SYSLOG)

find_package(Gperftools)
if(GPERFTOOLS_FOUND)
set(LIBS ${LIBS} ${GPERFTOOLS_LIBRARIES})
add_definitions(-DUSE_GPERFTOOLS)
link_directories(${GPERFTOOLS_LIBRARYRARY_DIRS})
endif(GPERFTOOLS_FOUND)

include_directories(${webservice_inc_dir})
include_directories(${webservice_src_dir})
include_directories(${webservice_inc_dir}/webservice)

if (BUILD_SHARED_LIBS)
add_library(${webservice_lib_name} SHARED ${webservice_source_files})
endif (BUILD_SHARED_LIBS)

if (BUILD_STATIC_LIBS)
add_library(${webservice_lib_name}-static STATIC ${webservice_source_files})
set_target_properties(${webservice_lib_name}-static PROPERTIES OUTPUT_NAME ${webservice_lib_name})
endif (BUILD_STATIC_LIBS)

set(CMAKE_SKIP_BUILD_RPATH TRUE)
#set(CMAKE_INSTALL_RPATH "$ORIGIN/../../lib")
#set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)



add_executable(http-server ${webservice_base_dir}/example/example.cc)
if (BUILD_STATIC_LIBS)
target_link_libraries(http-server ${webservice_lib_name}-static ${LIBS})
else (BUILD_STATIC_LIBS)
target_link_libraries(http-server ${webservice_lib_name} ${LIBS})
endif (BUILD_STATIC_LIBS)


add_executable(https-server ${webservice_base_dir}/example/example_https.cc)
if (BUILD_STATIC_LIBS)
target_link_libraries(https-server ${webservice_lib_name}-static ${LIBS})
else (BUILD_STATIC_LIBS)
target_link_libraries(https-server ${webservice_lib_name} ${LIBS})
endif (BUILD_STATIC_LIBS)

set(webservice_www_dir ${webservice_base_dir}/www)

if (BUILD_SHARED_LIBS)
install(TARGETS ${webservice_lib_name} DESTINATION lib)
endif (BUILD_SHARED_LIBS)

if (BUILD_STATIC_LIBS)
install(TARGETS ${webservice_lib_name}-static DESTINATION lib)
endif (BUILD_STATIC_LIBS)

install(FILES ${webservice_header_files} DESTINATION include/webservice)
install(PROGRAMS ${CMAKE_BINARY_DIR}/https-server DESTINATION bin/webservice/)
install(PROGRAMS ${CMAKE_BINARY_DIR}/http-server DESTINATION bin/webservice/)
install(PROGRAMS ${webservice_script_files} DESTINATION bin/webservice)
install(DIRECTORY ${webservice_www_dir} DESTINATION bin/webservice)
