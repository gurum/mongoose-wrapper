/*
 * websocket.h
 *
 *  Created on: Jun 10, 2013
 *      Author: buttonfly
 */

#ifndef _GURUM_WEBSOCKET_H_
#define _GURUM_WEBSOCKET_H_

#include <stdio.h>
#include <string.h>
#include <string>
#include <pthread.h>

struct mg_connection;

namespace gurum {

class WebSocket {
public:
	WebSocket(struct mg_connection* conn);
	virtual ~WebSocket();

	virtual std::string Read();
	virtual int Write(const std::string& msg);
	virtual int Write(const char* buf, size_t len);

	std::string RemoteIp(void* conn);

protected:
	const struct mg_connection* _conn;
};

} /* namespace k */

#endif /* _GURUM_WEBSOCKET_H_ */

