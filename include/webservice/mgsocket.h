/*
 * mgsocket.h
 *
 *  Created on: Feb 28, 2015
 *      Author: buttonfly
 */

#ifndef _GURUM_MGSOCKET_H_
#define _GURUM_MGSOCKET_H_


#include <stdio.h>
#include <string.h>
#include <string>
#include <pthread.h>

struct mg_connection;

namespace gurum {

class MGSocket {
public:
	MGSocket(struct mg_connection* conn);
	virtual ~MGSocket();

	virtual std::string Read();
	virtual int Write(const std::string& msg);
	virtual int Write(const char* buf, size_t len);

	void SetHeader(const char* name, const char* v);

	std::string Get(std::string name);

	std::string RemoteIp(void* conn);

protected:
	const struct mg_connection* _conn;
};

} /* namespace gurm */

#endif /* _GURUM_MGSOCKET_H_ */
