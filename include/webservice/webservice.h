/*
 * webservice.h
 *
 *  Created on: Jun 8, 2013
 *      Author: buttonfly
 */

#ifndef _GURUM_WEBSERVICE_H_
#define _GURUM_WEBSERVICE_H_

#define REST_SUPPORT	1

#include <string>
#include <map>
#if REST_SUPPORT
#endif
#include <list>
#include <thread>
#include <mutex>
#include <functional>

namespace gurum {

class WebService {
public:
  using OnService=std::function<int(struct mg_connection *, const std::string &url)>;

  using OnWebsocketConnected=std::function<int(struct mg_connection *)>;
  using OnWebsocketDisconnected=std::function<int(struct mg_connection *)>;

	WebService();
	virtual ~WebService();

	//@deprecated
	int init(void);

	int Init(const char *options[]);
	int Start(void);
	int Stop(void);

	int ReadAndDispatch();

	// @deprecated
//	int Add(HttpService* s);
//  int Remove(HttpService* s);

  void RegisterService(const std::string &url, OnService callback);
  void RegisterService(const char *url, OnService callback);
  void UnregisterService(const std::string &url);


  void SetOnWebsocketConnected(OnWebsocketConnected arg){on_websocket_connected_=arg;}
  void SetOnWebsocketDisconnected(OnWebsocketDisconnected arg){on_websocket_disconnected_=arg;}

	int Response(void *conn, std::string& msg, uint8_t key[4]);

	int lock(void);
	int unlock(void);

private:
  int exec(const std::string &url, struct mg_connection *conn);

	static int eventHandler(void *conn, int event);
	static int iterateCallback(void *c, int event);

	int fireSocketClosedEvent(void* conn);

	void* _context;

#if REST_SUPPORT
//	std::map<std::string, HttpService*> _httpservices;
//	HttpServiceHandler _httpserviceHandler;

  std::map<const std::string, OnService> services_;

  OnWebsocketConnected on_websocket_connected_=nullptr;
  OnWebsocketDisconnected on_websocket_disconnected_=nullptr;

#endif
	std::thread _thread;
	bool _stopped = false;
	std::recursive_mutex lck_;
};

} /* namespace k */
#endif /* _GURUM_WEBSERVICE_H_ */
