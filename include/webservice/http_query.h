/*
 * HttpQuery.h
 *
 *  Created on: Oct 16, 2015
 *      Author: buttonfly
 */

#ifndef _GURUM_HTTPQUERY_H_
#define _GURUM_HTTPQUERY_H_

#include <string>
#include <map>
#include <json/json.h>

#include "mgsocket.h"

#ifndef YES
#define YES true
#endif

#ifndef NO
#define NO false
#endif

#define OK		0
#define NOT_OK	-1

namespace gurum {

class HttpQuery {
public:
	HttpQuery(const char* query);
	virtual ~HttpQuery();

	auto Parse() -> bool;
	auto ToJson() -> std::string;

	static auto REST_Parse(Json::Value &root, struct mg_connection * conn) -> bool;
	static auto REST_Response(Json::Value &res, struct mg_connection * conn) -> int;

private:
	typedef std::map<std::string, std::string> Dictionary;
	Dictionary _kv;
	const char *_query;
};

} /* namespace k */

#endif /* LIBWEBSERVER_HTTPQUERY_H_ */
