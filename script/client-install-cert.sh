#!/bin/bash

cert="$1"

[ ! -d /usr/share/ca-certificates/extra ] && mkdir -p /usr/share/ca-certificates/extra

if [ ! -f "/usr/share/ca-certificates/extra/$1" ]; then
	cp $cert /usr/share/ca-certificates/extra/
fi

if ! grep -q "$1" "/etc/ca-certificates.conf"; then
	echo "extra/$1" >> /etc/ca-certificates.conf
	update-ca-certificates
fi

echo "[Notice] Add $cert in /etc/ca-certificates.conf"
echo "[Notice] Then, run [dpkg-reconfigure ca-certificates]"
echo "[Notice] After that, try [https -v $YOUR-ADDRESS]"

