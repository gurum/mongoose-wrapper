#!/bin/bash

cn="$1"
out=$cn.crt

openssl req \
        -x509 \
        -nodes \
        -days 365 \
        -newkey rsa:2048 \
        -keyout $cn.key \
        -out $out \
        -subj "/C=KR/ST=Seoul/CN=$cn" \
        -config <(cat /usr/lib/ssl/openssl.cnf \
        <(printf "[SAN]\nsubjectAltName=DNS.1:$cn")) \
        -reqexts SAN \
        -extensions SAN

openssl x509 -text -noout -in $out

cat $cn.key > ssl.pem; cat $cn.crt >> ssl.pem

if [ ! -d ssl ]; then
  mkdir ssl
fi

mv $cn.key $out ssl.pem ssl

echo "./start https-server -s ssl/ssl.pem -p 443 [-w www]"


