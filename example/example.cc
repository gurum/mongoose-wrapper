/*
 * main.cpp
 *
 *  Created on: Feb 2, 2016
 *      Author: buttonfly
 */



//#include "webservice/http_service.h"
#if JSONRPC
#include "webservice/Plugin.h"
#endif
#include <string>
#include "webservice/mongoose.h"
#include "webservice/webservice.h"
#include "webservice/websocket.h"
#if JSONRPC
#include "webservice/JsonWebSocket.h"
#endif
#include <iostream>
#include "webservice/http_query.h"
#include <sys/stat.h>
#include <unistd.h>
#include <getopt.h>
#include "log_message.h"
#include <memory>

using namespace gurum;
#if JSONRPC
using namespace Json::Rpc;
#endif

static int daemon_init(void);
static void usage(const char* program);

struct LocalContext {
  bool is_daemon;
  std::string port;
  std::string html_root;
};

static void decode_options(int argc, char *argv[], struct LocalContext &context);


int main(int argc, char *argv[]) {

  struct LocalContext context;
  context.is_daemon=false;

  const char *kDefaultHtmlRoot  = ".";
  const char *kDefaultServicePort  =  "5000";

  decode_options(argc, argv, context);

  const char *port = context.port.empty() ? kDefaultServicePort : context.port.c_str();
  const char *html_root = context.html_root.empty() ? kDefaultHtmlRoot : context.html_root.c_str();

  if(context.is_daemon)
    daemon_init();

  std::unique_ptr<WebSocket> websocket;
	std::unique_ptr<WebService> webservice{new WebService};
	const char *options[] = {
			"listening_port",  port,
			"document_root",html_root,
			NULL };

	webservice->Init(options);

	// add plugin
#if JSONRPC
	webservice.add((IPlugin*) foo);
#endif
//	webservice.Add((HttpService*) foo);
	webservice->RegisterService("/foo2",  [](struct mg_connection *conn, const std::string &url)->int{
      Json::Value root;
      Json::Value res;

      if(HttpQuery::REST_Parse(root, conn) != YES) {
        res["result"] = -1;
        HttpQuery::REST_Response(res, conn);
        return OK;
      }

      std::string method(conn->request_method);
      if(method=="GET") {
        //TODO:
        res["msg"] = "You called a RESTful API: Foo2 in the GET method.";
        res["result"] = 0;
      }
      else if(method=="PUT") {
        auto uri = root["status"].asString();
        if(uri.empty()) {
          // default
          uri = "video://0";
        }
        //TODO: record check
        res["msg"] = "You called a RESTful API: Foo2 in the PUT method.";
        res["result"] = 0;
      }
      else if(method=="POST") {
        //TODO
        res["msg"] = "You called a RESTful API: Foo2 in the POST method.";
        res["result"] = 0;
      }
      else if(method=="DELETE") {
        //TODO:
        res["msg"] = "You called a RESTful API: Foo2 in the DELETE method.";
        res["result"] = 0;
      }
      HttpQuery::REST_Response(res, conn);
      return 0;
	});

	webservice->SetOnWebsocketConnected([&](struct mg_connection *conn)->int{
	  LOG(INFO) << " ";
	  websocket.reset(new WebSocket(conn));
	});

  webservice->SetOnWebsocketDisconnected([&](struct mg_connection *conn)->int{
    LOG(INFO) << " ";
    websocket.reset();
  });


	webservice->Start();

	for(;;) {
		std::string in;
		std::getline (std::cin, in);

		if(! websocket) continue;

    Json::Value res;
    res["data"]=in;

    Json::StreamWriterBuilder builder;
    builder["commentStyle"] = "None";
    builder["indentation"] = "   ";  // or whatever you like
    std::unique_ptr<Json::StreamWriter> writer{builder.newStreamWriter()};

    std::ostringstream stream;
    writer->write(res, &stream);
    auto body = stream.str();
    websocket->Write(body);

#if deprecated
    Json::FastWriter writer;
    auto body = writer.write(res);
    websocket->Write(body);
#endif

		#if JSONRPC
		foo->SendEvent(in);
		#endif
//		sleep(1);
	}
	webservice->Stop();
	printf("terminated\n");
	return 0;
}


int daemon_init(void) {
  pid_t pid;
  if ((pid = fork()) < 0) {
    return -1;
  } else if (pid != 0) {
    exit(0);
  }
  setsid();
  umask(0);
  return 0;
}

//static
void decode_options(int argc, char *argv[], struct LocalContext &context) {
        static const char *opt_string = "p:dh";
        static struct option const longopts[] = {
                        { "port", required_argument, NULL,'p' },
                        { "help", no_argument, NULL, 'h' },
                        { "daemon", no_argument, NULL, 'h' },
                        { NULL, 0, NULL, 0 } };
    int optc, longind=0;
    const char *name = argv[0];
    while((optc=getopt_long(argc,argv,opt_string,longopts,&longind))!=-1) {
        switch (optc) {
        case 'h': {
          usage(name);
          exit(0);
        }
        case 'p': {
          context.port=optarg;
          break;
        }
        case 'd': {
          context.is_daemon=true;
          break;
        }
        default: {
          usage(name);
          exit(0);
        }}
    }
   for(int i = optind; i < argc; i++) {
        fprintf(stderr, "argv[%d]: %s\n", i, argv[i]);
        context.html_root=argv[i];
        // context.setAlias(argv[i]);
    }
}


//static
void usage(const char* program) {
        printf("Usage: %s <options>\n", program);
        printf("Options:\n"
                        "\t-h, -help\n"
                        "\t\tPrint this help\n"
                        "\t-b, --broker\n"
                        "\t\tmqtt-broker\n"
            "\t-d, --daemon\n"
            "\t\tdaemon\n"
            "\t-p, --port <1~65525>\n"
                        "\t\tport. It might be a file if it is unix domain sock.\n"
                        "\tex: %s lo -p 8082\n"
                        "\tex: %s wlan0 -p 8082\n", program ,program
        );
}
