# Generate ssl keys
cd $TARGET_DIR/usr/local/bin
```
./server-gen-keys.sh <your-domain>
```

# Run https server
```
./start https-server -p 443 -ssl ssl/ssl.pem
```

Get <your-cert> in another local system.

# Client
```
./client-install-cert.sh <your-cert>.crt
```

For a browser access
```
./client-update-nss.sh <your-cert>.crt
```





# Makefie example

```
SHELL := /bin/bash

include $(BUILD_ROOT)/tools/config.mk
include $(BUILD_ROOT)/tools/tools.mk

PWD := ${shell pwd}
OPENSOURCE ?= ${PWD}/..

TARGET_DIR ?= $(PLATFORM_ROOT)/target
TARGET_INC_DIR = $(TARGET_DIR)/usr/local/include
TARGET_LIB_DIR = $(TARGET_DIR)/usr/local/lib

REQUIRED_CFLAGS = "-I$(TARGET_INC_DIR)"
#REQUIRED_CXXFLAGS += -I$(TARGET_INC_DIR)/jsoncpp
REQUIRED_LDFLAGS = "-L$(TARGET_LIB_DIR) -ljsoncpp -luriparser"

VENDOR_NAME := webservice
VENDOR_TOP := $(OPENSOURCE)/$(VENDOR_NAME)
VENDOR_VER := 1.0.0
#VENDOR_DIR := $(VENDOR_TOP)/$(VENDOR_NAME)-$(VENDOR_VER)
VENDOR_DIR := $(VENDOR_TOP)/$(VENDOR_NAME)
VENDOR_SOURCE_TARBALL := $(VENDOR_TOP)/$(VENDOR_NAME)-$(VENDOR_VER).tar.gz


.PHONY : $(VENDOR_NAME)

all: $(VENDOR_NAME)-source $(VENDOR_NAME)-config $(VENDOR_NAME)

$(VENDOR_NAME)-source:
	@if ! [ -e "$(VENDOR_DIR)/.unpacked" ]; then \
	cd $(VENDOR_TOP); \
	git clone https://gitlab.com/gurum/mongoose-wrapper.git $(VENDOR_NAME); \
	touch $(VENDOR_DIR)/*; \
	touch $(VENDOR_DIR)/.unpacked; \
	fi

$(VENDOR_NAME):
	@echo "================ build ${VENDOR_NAME}"
	cd $(VENDOR_DIR)/build ;	\
	$(MAKE) VERBOSE=1 ;	\
	$(MAKE) install DESTDIR=$(TARGET_DIR)

clean:
	@echo "================ clean ${VENDOR_NAME}"
	$(RM) -f $(VENDOR_TOP)/.configured
	$(RM) -rf $(VENDOR_DIR)
	$(call DISPLAY_RED ,##  $@  COMPLETE !!!  )

$(VENDOR_NAME)-config:
	@if ! [ -d $(VENDOR_DIR)/build ]; then \
		mkdir $(VENDOR_DIR)/build;	\
		cd $(VENDOR_DIR)/build && cmake -DCMAKE_PREFIX_PATH=$(TARGET_DIR)/usr/local ..;		\
		touch $(VENDOR_TOP)/.configured;		\
	fi

install:

```
